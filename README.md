# OpenML dataset: HSI-Futures

https://www.openml.org/d/43578

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This is a historical data of HangSeng Futures Index based in Hong Kong.
For non traders, the data is a time-series (sequential flow of numbers) describing the HangSeng Futures Index of HongKong.
Every minute one line of data is created. 
Each line has :
open : first price at start of that minute
high : highest price during that minute
low : lowest price during that minute
close : last price for that minute time frames
volume : total number of units traded 
This is can be called 'raw data' on a 1 minute time frame.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43578) of an [OpenML dataset](https://www.openml.org/d/43578). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43578/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43578/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43578/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

